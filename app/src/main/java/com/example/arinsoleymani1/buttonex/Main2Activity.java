package com.example.arinsoleymani1.buttonex;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {


    Button button3;
    TextView text3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        button3 = (Button) findViewById(R.id.button3);
        text3 = (TextView) findViewById(R.id.text3);

        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                text3.setBackgroundColor(Color.RED);
            }

        });
    }
}
