package com.example.arinsoleymani1.buttonex;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button button1;
    Button button2;
    TextView text1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //CHANGE TITLE ACTIVITY
        getSupportActionBar().setTitle("Activity Test");

        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        text1 = (TextView) findViewById(R.id.text1);

        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                    if (button1.getText() == "Hola") {
                        button1.setText("World");
                    }else {
                        button1.setText("Hola");
                    }
                }

        });

        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (text1.getText() == "First text") {
                    text1.setText("Second");
                }else {
                    text1.setText("First text");
                }
            }

        });
    }


}

